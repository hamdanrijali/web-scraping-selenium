package com.codetriage.scraper;

import com.codetriage.scraper.dto.FarmasiDto;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Our scraper class
 */
public class App {

    /**
     * The main method of our class, which will also house the scraping
     * functionality.
     */
    public static void main(String[] args) throws InterruptedException, IOException {

//    try {
//      /**
//       * Here we create a document object,
//       * The we use JSoup to fetch the website.
//       */
//      Document doc = Jsoup.connect("https://www.codetriage.com/?language=Java").get();
//
//      /**
//       * With the document fetched,
//       * we use JSoup???s title() method to fetch the title
//       */
//      System.out.printf("\nWebsite Title: %s\n\n", doc.title());
//
//
//      // Get the list of repositories
//      Elements repositories = doc.getElementsByClass("repo-item");
//
//      /**
//       * For each repository, extract the following information:
//       * 1. Title
//       * 2. Number of issues
//       * 3. Description
//       * 4. Full name on github
//       */
//      for (Element repository : repositories) {
//        // Extract the title
//        String repositoryTitle = repository.getElementsByClass("repo-item-title").text();
//
//        // Extract the number of issues on the repository
//        String repositoryIssues = repository.getElementsByClass("repo-item-issues").text();
//
//        // Extract the description of the repository
//        String repositoryDescription = repository.getElementsByClass("repo-item-description").text();
//
//        // Get the full name of the repository
//        String repositoryGithubName = repository.getElementsByClass("repo-item-full-name").text();
//
//        /**
//         * The repository full name contains brackets that we remove first
//         * before generating the valif Github link.
//         */
//        String repositoryGithubLink = "https://github.com/" + repositoryGithubName.replaceAll("[()]", "");
//
//        // Format and print the information to the console
//        System.out.println(repositoryTitle + " - " + repositoryIssues);
//        System.out.println("\t" + repositoryDescription);
//        System.out.println("\t" + repositoryGithubLink);
//        System.out.println("\n");
//
//      }
//
//    /**
//     * Incase of any IO errors, we want the messages
//     * written to the console
//     */
//    } catch (IOException e) {
//      e.printStackTrace();
//    }

//    try {
//      /**
//       * Here we create a document object,
//       * The we use JSoup to fetch the website.
//       */
//      Document doc = Jsoup.connect("https://www.codetriage.com/?language=Java").get();
//
//      /**
//       * With the document fetched,
//       * we use JSoup???s title() method to fetch the title
//       */
//      System.out.printf("\nWebsite Title: %s\n\n", doc.title());
//
//
//      // Get the list of repositories
//      Elements repositories = doc.getElementsByClass("repo-item");
//
//      /**
//       * For each repository, extract the following information:
//       * 1. Title
//       * 2. Number of issues
//       * 3. Description
//       * 4. Full name on github
//       */
//      for (Element repository : repositories) {
//        // Extract the title
//        String repositoryTitle = repository.getElementsByClass("repo-item-title").text();
//
//        // Extract the number of issues on the repository
//        String repositoryIssues = repository.getElementsByClass("repo-item-issues").text();
//
//        // Extract the description of the repository
//        String repositoryDescription = repository.getElementsByClass("repo-item-description").text();
//
//        // Get the full name of the repository
//        String repositoryGithubName = repository.getElementsByClass("repo-item-full-name").text();
//
//        /**
//         * The repository full name contains brackets that we remove first
//         * before generating the valif Github link.
//         */
//        String repositoryGithubLink = "https://github.com/" + repositoryGithubName.replaceAll("[()]", "");
//
//        // Format and print the information to the console
//        System.out.println(repositoryTitle + " - " + repositoryIssues);
//        System.out.println("\t" + repositoryDescription);
//        System.out.println("\t" + repositoryGithubLink);
//        System.out.println("\n");
//
//      }
//
//      /**
//       * Incase of any IO errors, we want the messages
//       * written to the console
//       */
//    } catch (IOException e) {
//      e.printStackTrace();
//    }

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://cekbpom.pom.go.id");
//    List<WebElement> element = driver.findElements(By.className("menu"));

        Actions actions = new Actions(driver);
        List<WebElement> menus = driver.findElements(By.className("menu"));
        actions.moveToElement(menus.get(2)).build().perform();

        List<WebElement> submenus = driver.findElements(By.className("submenu"));
        actions.moveToElement(submenus.get(1)).click().build().perform();



        List<WebElement> rows = driver.findElements(By.xpath("//body/div/div/div/form/table[contains(@class,'tabelajax')]/tbody/tr"));
//        Nomor Registrasi GKL1402350044A1
//        Tanggal Terbit 13-08-2019
//        Masa Berlaku s/d 13-08-2024
//        Diterbitkan Oleh Registrasi Obat
//        Direktorat Registrasi Obat
//        Produk Obat
//        Nama Produk CEFTAZIDIME PENTAHYDRATE
//        Bentuk Sediaan SERBUK INJEKSI 1,2706 G
//        Komposisi - CEFTAZIDIME BUFFERED WITH SODIUM CARBONATE
//        Merk -
//                Kemasan DUS, 10 VIAL @ 1 G
//        Pendaftar BERNOFARM - Indonesia - -


        int MaxRetries = 5;

        List<FarmasiDto> farmasiDtos = new ArrayList<>();
        for (int i = 2; i < rows.size(); i++) {
            FarmasiDto farmasiDto = new FarmasiDto();
            for (int j = 0; j < MaxRetries; j++) {
                try {
                    rows.get(i).click();
                    break;
                } catch (Exception e) {
                    Thread.sleep(100);
                }
            }

            Thread.sleep(500);
            WebElement contentTd = driver.findElement(By.id("newtr"));
//            String[] contentTdSplit = contentTd.getText().split("\n");

            Pattern replace = Pattern.compile("(Nomor Registrasi |Tanggal Terbit |Masa Berlaku s/d |Diterbitkan Oleh |Produk |Nama Produk |Bentuk Sediaan |Komposisi |Merk |Kemasan |Pendaftar |Diproduksi Oleh |Pemberi Lisensi )");
            Matcher matcher2 = replace.matcher(contentTd.getText());
            String[] contentTdSplit = matcher2.replaceAll("").split("\n");
            farmasiDto.setNoReg(contentTdSplit[0]);
            farmasiDto.setTglTerbit(contentTdSplit[1]);
            farmasiDto.setMasaBerlaku(contentTdSplit[2]);
            farmasiDto.setDiterbitkanOleh(contentTdSplit[3].concat(contentTdSplit[4]));
            farmasiDto.setProduk(contentTdSplit[5]);
            farmasiDto.setNamaProduk(contentTdSplit[6]);
            farmasiDto.setBentukSediaan(contentTdSplit[7]);
            farmasiDto.setKomposisi(contentTdSplit[8]);
            farmasiDto.setMerk(contentTdSplit[9]);
            farmasiDto.setKemasan(contentTdSplit[10]);
            farmasiDto.setPendaftaran(contentTdSplit[11]);

            farmasiDtos.add(farmasiDto);

//            Thread.sleep(2000);
        }


        Workbook wb = new HSSFWorkbook();
        OutputStream fileOut = new FileOutputStream("obat.xls");

        Sheet sheet = wb.createSheet("bpom_obat_Result");
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("noReg");
        header.createCell(1).setCellValue("tglTerbit");
        header.createCell(2).setCellValue("masaBerlaku");
        header.createCell(3).setCellValue("diterbitkanOleh");
        header.createCell(4).setCellValue("produk");
        header.createCell(5).setCellValue("namaProduk");
        header.createCell(6).setCellValue("bentukSediaan");
        header.createCell(7).setCellValue("komposisi");
        header.createCell(8).setCellValue("merk");
        header.createCell(9).setCellValue("kemasan");
        header.createCell(10).setCellValue("pendaftaran");
        int counter = 1;

        for (FarmasiDto farmasiEach : farmasiDtos) {
            Row row = sheet.createRow(counter++);
            row.createCell(0).setCellValue(farmasiEach.getNoReg());
            row.createCell(1).setCellValue(farmasiEach.getTglTerbit());
            row.createCell(2).setCellValue(farmasiEach.getMasaBerlaku());
            row.createCell(3).setCellValue(farmasiEach.getDiterbitkanOleh());
            row.createCell(4).setCellValue(farmasiEach.getProduk());
            row.createCell(5).setCellValue(farmasiEach.getNamaProduk());
            row.createCell(6).setCellValue(farmasiEach.getBentukSediaan());
            row.createCell(7).setCellValue(farmasiEach.getKomposisi());
            row.createCell(8).setCellValue(farmasiEach.getMerk());
            row.createCell(9).setCellValue(farmasiEach.getKemasan());
            row.createCell(10).setCellValue(farmasiEach.getPendaftaran());
        }

        wb.write(fileOut);

    }
}
