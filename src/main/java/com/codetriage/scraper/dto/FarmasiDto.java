package com.codetriage.scraper.dto;

public class FarmasiDto {

    private String noReg;
    private String tglTerbit;
    private String masaBerlaku;
    private String diterbitkanOleh;
    private String produk;
    private String namaProduk;
    private String bentukSediaan;
    private String komposisi;
    private String merk;
    private String kemasan;
    private String pendaftaran;

    public String getNoReg() {
        return noReg;
    }

    public void setNoReg(String noReg) {
        this.noReg = noReg;
    }

    public String getTglTerbit() {
        return tglTerbit;
    }

    public void setTglTerbit(String tglTerbit) {
        this.tglTerbit = tglTerbit;
    }

    public String getMasaBerlaku() {
        return masaBerlaku;
    }

    public void setMasaBerlaku(String masaBerlaku) {
        this.masaBerlaku = masaBerlaku;
    }

    public String getDiterbitkanOleh() {
        return diterbitkanOleh;
    }

    public void setDiterbitkanOleh(String diterbitkanOleh) {
        this.diterbitkanOleh = diterbitkanOleh;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getNamaProduk() {
        return namaProduk;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }

    public String getBentukSediaan() {
        return bentukSediaan;
    }

    public void setBentukSediaan(String bentukSediaan) {
        this.bentukSediaan = bentukSediaan;
    }

    public String getKomposisi() {
        return komposisi;
    }

    public void setKomposisi(String komposisi) {
        this.komposisi = komposisi;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getKemasan() {
        return kemasan;
    }

    public void setKemasan(String kemasan) {
        this.kemasan = kemasan;
    }

    public String getPendaftaran() {
        return pendaftaran;
    }

    public void setPendaftaran(String pendaftaran) {
        this.pendaftaran = pendaftaran;
    }
}
